﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SiteLocaly
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class LOCALYEntities : DbContext
    {
        public LOCALYEntities()
            : base("name=LOCALYEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Utilisateur> Utilisateur { get; set; }
        public virtual DbSet<Vendeur> Vendeur { get; set; }
        public virtual DbSet<Article> Article { get; set; }
        public virtual DbSet<LigneCommande> LigneCommande { get; set; }
        public virtual DbSet<Magasin> Magasin { get; set; }
        public virtual DbSet<Quantite> Quantite { get; set; }
        public virtual DbSet<Rayon> Rayon { get; set; }
        public virtual DbSet<RayonArticle> RayonArticle { get; set; }
        public virtual DbSet<Acheteur> Acheteur { get; set; }
        public virtual DbSet<Adresse> Adresse { get; set; }
        public virtual DbSet<Commande> Commande { get; set; }
    }
}
