﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Web;

namespace SiteLocaly.ViewModels
{
    public class MagasinViewModel
    { 
        public int id { get; set; }
        public string libelle { get; set; } 
        public decimal latitude { get; set; }
        public decimal longitude { get; set; }
        public List<ArticleViewModel> Articles {get;set;}
    }
}