﻿using SiteLocaly.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteLocaly.Controllers
{
    public class ConnexionController : Controller
    {
        LOCALYEntities Context = new LOCALYEntities();

        [HttpGet]
        public ActionResult Authentification()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Authentification(LoginModel model)
        {
            if(!ModelState.IsValid)
            {
                return View(model);
            } else
            {
                //Test
                if(model.Username == "Test" && model.Password == "Test")
                {
                    Session["UserID"] = Guid.NewGuid();
                    return RedirectToAction("Acceuil", "Home");
                } else
                {
                    //Check base
                    var encPass = Tools.Security.Encrypt(model.Password);
                    var user = Context.Utilisateur.SingleOrDefault(u => (u.motDePasse == encPass) && (u.prenom.ToUpper() + "." + u.nom.ToUpper() == model.Username.ToUpper()));
                    if(user != null)
                    {
                        Session["UserID"] = Guid.NewGuid();
                        return RedirectToAction("Acceuil", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("Erreur", "Mot de passe ou nom d'utilisateur erroné.");
                        return View(model);
                    }
                }
            }
        }
    }
}



