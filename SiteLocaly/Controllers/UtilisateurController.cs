﻿using SiteLocaly.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteLocaly.Controllers
{
    [UserAuthenticationFilter]
    public class UtilisateurController : Controller
    {
        //Contexte de la base de données
        LOCALYEntities Context = new LOCALYEntities();

        // GET: Utilisateur
        public ActionResult Index()
        {
            return View();
        }

        // GET: Utilisateur/Details/5
        public ActionResult Details(int id)
        {
            Utilisateur user = this.Context.Utilisateur.SingleOrDefault(u => u.id == id);
            return View(user);
        }

        // GET: Utilisateur/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Utilisateur/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            Utilisateur user = new Utilisateur();
            try
            {
                // TODO: Add insert logic here
                Context.Utilisateur.Add(user);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Utilisateur/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Utilisateur/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Utilisateur/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Utilisateur/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
