﻿using SiteLocaly.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteLocaly.Controllers
{
    [UserAuthenticationFilter]
    public class MagasinController : Controller
    {
        LOCALYEntities Context = new LOCALYEntities();

        // GET: Magasin
        public ActionResult Index()
        {
            return View();
        }

        //GET: Magasin/Article/1
        public ActionResult Article(int id)
        {
            Magasin mag = Context.Magasin.SingleOrDefault(m => m.id == id);

            return View(mag);
        }

        // GET: Magasin/Details/5
        public ActionResult Details(int id)
        {

            Magasin mag = Context.Magasin.SingleOrDefault(m => m.id == id);

            return View(mag);
        }

        // GET: Magasin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Magasin/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Magasin/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Magasin/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Magasin/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Magasin/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
