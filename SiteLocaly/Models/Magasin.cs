﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteLocaly
{
    public partial class Magasin
    {
        public Adresse getAdresse()
        {
            return this.Adresse;
        }

        public List<decimal> getCoordonnees()
        {
            var lst = new List<decimal>();

            if(this.Adresse.latitude != null && this.Adresse.longitude != null)
            {
                lst.Add(this.Adresse.latitude);
                lst.Add(this.Adresse.longitude);
            } else
            {
                //Erreur une des deux vals est null

            }

            return lst;
        }
    }
}