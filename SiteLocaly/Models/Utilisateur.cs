﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteLocaly
{
    public partial class Utilisateur
    {
        public List<Acheteur> getComptesClients()
        {
            if (this.Acheteur != null)
            {
                return Acheteur.ToList();
            }

            else
                return null;
        }

        public List<Vendeur> getComptesVendeur()
        {
            if (this.Vendeur != null)
            {
                return Vendeur.ToList();
            }

            else
                return null;
        }
    }
}