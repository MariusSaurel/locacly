﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteLocaly.Models
{
    public class LoginModel
    {
        [Display(Name = "Utilisateur")]
        [Required]
        public string Username { get; set; }

        [Display(Name = "Mot de passe")]
        [Required]
        public string Password { get; set; }
    }
}
